-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 06, 2021 at 01:53 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.3.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_fullcalendar`
--

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `venue_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `name`, `start_time`, `end_time`, `status`, `created_at`, `updated_at`, `deleted_at`, `venue_id`) VALUES
(1, 'test', '2021-07-08 19:50:14', NULL, NULL, '2021-07-08 03:50:21', '2021-07-12 02:08:29', '2021-07-12 02:08:29', 1),
(2, 'gab', '2021-07-08 19:54:18', NULL, NULL, '2021-07-08 03:54:26', '2021-07-12 02:08:27', '2021-07-12 02:08:27', 1),
(3, NULL, '2021-07-08 19:55:00', NULL, NULL, '2021-07-08 03:55:33', '2021-07-12 02:08:25', '2021-07-12 02:08:25', 1),
(4, 'gabgab', '2021-07-08 19:55:00', NULL, NULL, '2021-07-08 03:55:51', '2021-07-12 02:08:24', '2021-07-12 02:08:24', 1),
(5, 'bag', '2021-07-08 19:55:00', NULL, NULL, '2021-07-08 03:56:15', '2021-07-12 02:08:22', '2021-07-12 02:08:22', 1),
(6, 'seminar', '2021-07-12 18:51:13', NULL, NULL, '2021-07-12 02:51:29', '2021-07-13 03:15:20', '2021-07-13 03:15:20', 2),
(7, 'Advising', '2021-07-12 18:52:00', NULL, NULL, '2021-07-12 02:52:04', '2021-07-13 03:15:20', '2021-07-13 03:15:20', 2),
(8, NULL, NULL, NULL, NULL, '2021-07-12 02:54:14', '2021-07-12 02:54:18', '2021-07-12 02:54:18', NULL),
(9, 'Holiday', '2021-07-19 12:00:00', '2021-07-20 00:00:00', '9', '2021-07-19 05:29:45', '2021-07-19 05:29:45', NULL, NULL),
(10, 'Holiday', '2021-07-23 00:00:00', '2021-07-24 00:00:00', '9', '2021-07-20 03:17:59', '2021-07-20 03:17:59', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `meetings`
--

CREATE TABLE `meetings` (
  `id` int(10) UNSIGNED NOT NULL,
  `attendees` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `filer_id` int(255) DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `status` binary(1) DEFAULT NULL,
  `link` varchar(6535) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `meetings`
--

INSERT INTO `meetings` (`id`, `attendees`, `filer_id`, `end_time`, `start_time`, `status`, `link`, `created_at`, `updated_at`, `deleted_at`) VALUES
(149, NULL, NULL, NULL, '2021-07-18 08:58:27', NULL, NULL, '2021-07-17 17:11:29', '2021-07-19 05:07:29', '2021-07-19 05:07:29'),
(150, '1', 4, NULL, '2021-07-18 09:24:09', NULL, NULL, '2021-07-17 17:24:31', '2021-07-19 05:07:29', '2021-07-19 05:07:29'),
(151, '1', 5, NULL, '2021-07-18 09:25:06', NULL, NULL, '2021-07-17 17:25:09', '2021-07-19 05:07:29', '2021-07-19 05:07:29'),
(152, '1', 4, NULL, NULL, NULL, NULL, '2021-07-17 17:25:15', '2021-07-19 05:07:29', '2021-07-19 05:07:29'),
(153, '1', 5, NULL, NULL, NULL, NULL, '2021-07-17 17:25:15', '2021-07-19 05:07:29', '2021-07-19 05:07:29'),
(154, '1', 4, NULL, '2021-07-24 09:25:20', NULL, NULL, '2021-07-17 17:25:25', '2021-07-19 05:07:29', '2021-07-19 05:07:29'),
(155, '1', 5, NULL, '2021-07-24 09:25:20', NULL, NULL, '2021-07-17 17:25:25', '2021-07-19 05:07:29', '2021-07-19 05:07:29'),
(156, '4', 5, NULL, '2021-07-18 09:25:41', 0x31, NULL, '2021-07-17 17:25:43', '2021-07-19 04:59:52', '2021-07-19 04:59:52'),
(157, '4', 5, NULL, '2021-07-19 20:57:23', NULL, NULL, '2021-07-19 04:57:26', '2021-07-19 04:59:49', '2021-07-19 04:59:49'),
(158, '4', 5, NULL, '2021-07-19 21:00:05', 0x31, NULL, '2021-07-19 05:00:08', '2021-07-19 05:07:29', '2021-07-19 05:07:29'),
(159, '4', 5, NULL, '2021-07-21 21:00:42', 0x31, NULL, '2021-07-19 05:00:45', '2021-07-19 05:07:29', '2021-07-19 05:07:29'),
(160, '4', 5, NULL, '2021-07-19 21:07:43', 0x31, NULL, '2021-07-19 05:07:45', '2021-09-06 03:42:32', NULL),
(161, '6', 5, '2021-07-21 16:00:00', '2021-07-21 12:00:00', 0x31, NULL, '2021-07-20 01:33:26', '2021-09-06 03:49:06', NULL),
(162, '6', 5, '2021-07-21 13:00:00', '2021-07-21 09:00:00', 0x32, 'aaaaaaaaaaaa', NULL, '2021-09-06 03:51:03', NULL),
(163, '1', NULL, '2021-09-03 18:21:00', '2021-09-03 18:17:00', 0x31, NULL, '2021-09-06 02:17:59', '2021-09-06 02:17:59', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_100000_create_password_resets_table', 1),
(2, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(3, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(4, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(5, '2016_06_01_000004_create_oauth_clients_table', 1),
(6, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(7, '2019_11_13_000001_create_permissions_table', 1),
(8, '2019_11_13_000002_create_roles_table', 1),
(9, '2019_11_13_000003_create_users_table', 1),
(10, '2019_11_13_000004_create_venues_table', 1),
(11, '2019_11_13_000005_create_events_table', 1),
(12, '2019_11_13_000006_create_meetings_table', 1),
(13, '2019_11_13_000007_create_permission_role_pivot_table', 1),
(14, '2019_11_13_000008_create_role_user_pivot_table', 1),
(15, '2019_11_13_000009_add_relationship_fields_to_events_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `title`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'user_management_access', NULL, NULL, NULL),
(2, 'permission_create', NULL, NULL, NULL),
(3, 'permission_edit', NULL, NULL, NULL),
(4, 'permission_show', NULL, NULL, NULL),
(5, 'permission_delete', NULL, NULL, NULL),
(6, 'permission_access', NULL, NULL, NULL),
(7, 'role_create', NULL, NULL, NULL),
(8, 'role_edit', NULL, NULL, NULL),
(9, 'role_show', NULL, NULL, NULL),
(10, 'role_delete', NULL, NULL, NULL),
(11, 'role_access', NULL, NULL, NULL),
(12, 'user_create', NULL, NULL, NULL),
(13, 'user_edit', NULL, NULL, NULL),
(14, 'user_show', NULL, NULL, NULL),
(15, 'user_delete', NULL, NULL, NULL),
(16, 'user_access', NULL, NULL, NULL),
(17, 'venue_create', NULL, NULL, NULL),
(18, 'venue_edit', NULL, NULL, NULL),
(19, 'venue_show', NULL, NULL, NULL),
(20, 'venue_delete', NULL, NULL, NULL),
(21, 'venue_access', NULL, NULL, NULL),
(22, 'event_create', NULL, NULL, NULL),
(23, 'event_edit', NULL, NULL, NULL),
(24, 'event_show', NULL, NULL, NULL),
(25, 'event_delete', NULL, NULL, NULL),
(26, 'event_access', NULL, NULL, NULL),
(27, 'meeting_create', NULL, NULL, NULL),
(28, 'meeting_edit', NULL, NULL, NULL),
(29, 'meeting_show', NULL, NULL, NULL),
(30, 'meeting_delete', NULL, NULL, NULL),
(31, 'meeting_access', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`role_id`, `permission_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10),
(1, 11),
(1, 12),
(1, 13),
(1, 14),
(1, 15),
(1, 16),
(1, 17),
(1, 18),
(1, 19),
(1, 20),
(1, 21),
(1, 22),
(1, 23),
(1, 24),
(1, 25),
(1, 26),
(1, 27),
(1, 28),
(1, 29),
(1, 30),
(1, 31),
(2, 17),
(2, 18),
(2, 19),
(2, 20),
(2, 21),
(2, 22),
(2, 23),
(2, 24),
(2, 25),
(2, 26),
(2, 27),
(2, 28),
(2, 29),
(2, 30),
(2, 31),
(3, 19),
(5, 27),
(5, 29),
(5, 31),
(4, 27),
(4, 29),
(4, 31),
(4, 28),
(4, 30);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `title`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Admin', NULL, NULL, NULL),
(2, 'User', NULL, '2021-07-12 02:09:58', '2021-07-12 02:09:58'),
(3, 'test', '2021-07-08 04:42:18', '2021-07-12 02:09:20', '2021-07-12 02:09:20'),
(4, 'Student', '2021-07-12 02:10:39', '2021-07-12 02:10:45', NULL),
(5, 'Professor', '2021-07-12 02:19:01', '2021-07-12 02:19:01', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4),
(5, 5),
(6, 4);

-- --------------------------------------------------------

--
-- Table structure for table `schedules`
--

CREATE TABLE `schedules` (
  `id` int(10) UNSIGNED NOT NULL,
  `attendees` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `filer_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `dow` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` binary(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `schedules`
--

INSERT INTO `schedules` (`id`, `attendees`, `filer_id`, `end_time`, `start_time`, `dow`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Capstone Class', '5', '13:00:00', '10:00:00', '1,4', 0x37, NULL, NULL, NULL),
(3, 'TVA', '5', '10:00:00', '12:00:00', '2,3', 0x37, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `status_code`
--

CREATE TABLE `status_code` (
  `id` int(11) NOT NULL,
  `status_name` varbinary(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `status_code`
--

INSERT INTO `status_code` (`id`, `status_name`) VALUES
(1, 0x50656e64696e67);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` datetime DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Admin', 'admin@admin.com', NULL, '$2y$10$zPiaTbYwkxYcejFmEimhWedeAogTJvEb/yGmBVx390ihhPFy8r896', NULL, NULL, NULL, NULL),
(2, 'test', 'test@test.com', NULL, '$2y$10$7sQ1uAoHWq/ErVLuDIAFbuHESdEY6tQGq6Wl7Y1DJRKlDecC8e28y', NULL, '2021-07-08 04:03:29', '2021-07-12 02:04:55', '2021-07-12 02:04:55'),
(3, 'test non admin', 'test1@test.com', NULL, '$2y$10$puhVe2UM84JwkU/Gqfl35.PyNo5qYHP..U202w//zZ7qpsaCpCHK6', NULL, '2021-07-08 04:43:03', '2021-07-12 02:04:52', '2021-07-12 02:04:52'),
(4, 'Student Test', 'student@test.com', NULL, '$2y$10$zPiaTbYwkxYcejFmEimhWedeAogTJvEb/yGmBVx390ihhPFy8r896', NULL, '2021-07-12 02:11:13', '2021-07-12 02:11:13', NULL),
(5, 'Professor Test', 'professor@test.com', NULL, '$2y$10$zPiaTbYwkxYcejFmEimhWedeAogTJvEb/yGmBVx390ihhPFy8r896', NULL, '2021-07-12 02:17:42', '2021-07-12 02:17:42', NULL),
(6, 'Student Test 2', 'gjcalida@gmail.com', NULL, '$2y$10$EphoCYLmLtV8wLfEpPLseu2wNCk7mYf/ZTUL0fYLx6OCEiuvcKM0S', NULL, '2021-07-20 01:27:42', '2021-07-20 01:27:42', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `venues`
--

CREATE TABLE `venues` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `venues`
--

INSERT INTO `venues` (`id`, `name`, `address`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'test', 'test4e4st', '2021-07-08 03:50:05', '2021-07-12 02:11:47', '2021-07-12 02:11:47'),
(2, 'Seminar', 'Google Meets', '2021-07-12 02:49:04', '2021-07-13 02:20:52', '2021-07-13 02:20:52');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`),
  ADD KEY `venue_fk_598776` (`venue_id`);

--
-- Indexes for table `meetings`
--
ALTER TABLE `meetings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD KEY `role_id_fk_598752` (`role_id`),
  ADD KEY `permission_id_fk_598752` (`permission_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD KEY `user_id_fk_598761` (`user_id`),
  ADD KEY `role_id_fk_598761` (`role_id`);

--
-- Indexes for table `schedules`
--
ALTER TABLE `schedules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status_code`
--
ALTER TABLE `status_code`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `venues`
--
ALTER TABLE `venues`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `meetings`
--
ALTER TABLE `meetings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=164;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `schedules`
--
ALTER TABLE `schedules`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `status_code`
--
ALTER TABLE `status_code`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `venues`
--
ALTER TABLE `venues`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `events`
--
ALTER TABLE `events`
  ADD CONSTRAINT `venue_fk_598776` FOREIGN KEY (`venue_id`) REFERENCES `venues` (`id`);

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_id_fk_598752` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_id_fk_598752` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_id_fk_598761` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_id_fk_598761` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
