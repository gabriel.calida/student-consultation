<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Venue;
use Carbon\Carbon;
use App\Meeting;
use App\Schedule;
use App\User;
use Auth;
use DB;
class SystemCalendarController extends Controller
{
    public $sources = [

        [
            'model'      => '\\App\\Schedule',
            'date_field' => 'start_time',
            'end_field' => 'end_time',
            'field'      => 'attendees',
            'filer_id'      => 'name',
            'prefix'     => 'Class',
            'suffix'     => '',
            'status'     => 'status',
            'dow'        => 'dow',
            'route'      => 'schedule',
            'start_range'=> null,
            'end_range'  => null,
   
        ],

        [
            'model'      => '\\App\\Event',
            'date_field' => 'start_time',
            'end_field' => 'end_time',
            'field'      => 'name',
            'filer_id'      => null,
            'prefix'     => 'event',
            'suffix'     => '',
            'status'     => 'status',
            'dow'        => null,
            'route'      => 'events',
            'start_range'=> null,
            'end_range'  => null,
   
        ],

        [
            'model'      => '\\App\\Meeting',
            'date_field' => 'start_time',
            'end_field' => 'end_time',
            'field'      => 'name',
            'filer_id'      => null,
            'prefix'     => 'Consultation - ',
            'suffix'     => '',
            'status'     => 'status',
            'dow'        => null,
            'route'      => 'meetings',
            'start_range'=> null,
            'end_range'  => null,


        ],
    ];

    public function index()
    {
        $events = [];
        $user = Auth::user();
        $userID = Auth::id();
        $prof_view = User::select('*');
        // dd($user['id']);
        $VerifyRole = DB::table('role_user')->select('role_id')
        ->where('user_id',$user['id'])
        ->first();
     $calendarEvents = Schedule::select('schedules.*')->get();
     // dd($calendarEvents);
        // dd($VerifyRole);
        $venues = Venue::all();
        if ($VerifyRole->role_id == 5) {
            // code...
        }
        $users= User::all();
        // dd($users);
        $requestIdPicked = request('user_id');
        // dd($requestIdPicked); 
        // dd(request('user_id'));

        foreach ($this->sources as $source) {
            if ($source['model'] == '\App\Event') {
                 $calendarEvents = $source['model']::when(request('venue_id') && $source['model'] == '\App\Event', function($query) {
                return $query->where('venue_id', request('venue_id'));
            })->get();
            }
            elseif ($source['model'] == '\App\Schedule') {
                if ($VerifyRole->role_id == 5) {

                    $calendarEvents = Schedule::select('schedules.*')
                ->join('users','users.id','=','schedules.filer_id')
                ->select('users.name as name','schedules.start_time','schedules.end_time','schedules.id','schedules.status','schedules.attendees','schedules.dow')
                ->where('filer_id',$user['id'])->get();

                }elseif ($VerifyRole->role_id == 4){

                    $calendarEvents = Schedule::when(request('user_id') != null ,function ($query) {
                        $query->select('schedules.*')
                        ->join('users','users.id','=','schedules.filer_id')
                         ->select('users.name as name','schedules.start_time','schedules.end_time','schedules.id','schedules.status','schedules.attendees','schedules.dow')
                        ->where('schedules.filer_id', request('user_id'));
                        })->get();


                }elseif($VerifyRole->role_id == 1){
                    $calendarEvents = Schedule::when(request('user_id') != null ,function ($query) {
                        $query->select('schedules.*')
                        ->join('users','users.id','=','schedules.filer_id')
                         ->select('users.name as name','schedules.start_time','schedules.end_time','schedules.id','schedules.status','schedules.attendees','schedules.dow');
                        })->get();
                }
                
                // dd($calendarEvents)
            }
            elseif ($source['model'] == '\App\Meeting') {
                if ($VerifyRole->role_id == 4) {
                    $calendarEvents = Meeting::select('meetings.*')
                ->join('users as test1', 'test1.id', '=', 'meetings.filer_id')
                ->select('test1.name as name','meetings.start_time','meetings.end_time','meetings.id','meetings.status')
                ->where('attendees',$user['id'])->get();
                }
                elseif ($VerifyRole->role_id == 5) {
                    $calendarEvents = Meeting::select('meetings.*')
                ->join('users as test1', 'test1.id', '=', 'meetings.attendees')
                ->select('test1.name as name','meetings.start_time','meetings.end_time','meetings.id','meetings.status')
                ->where('filer_id',$user['id'])->get();
                }elseif ($VerifyRole->role_id == 1) {
                    $calendarEvents = Meeting::select('meetings.*')
                ->join('users as test1', 'test1.id', '=', 'meetings.attendees')
                ->select('test1.name as name','meetings.start_time','meetings.end_time','meetings.id','meetings.status')
                ->get();
                }
                
                
            }

            // $calendarEvents = $source['model']::select('meetings.*')->where('attendees','6')->get();

            
            foreach ($calendarEvents as $model) {
                $crudFieldValue = $model->getOriginal($source['date_field']);
                $crudFieldValue1 = $model->getOriginal($source['end_field']);

                if (!$crudFieldValue) {
                    continue;
                }

                $events[] = [
                    'title' => trim($source['prefix'] . "\n " . $model->{$source['field']} ." " .$model->{$source['filer_id']}
                        . " " . $source['suffix']),
                    'start' => $crudFieldValue,
                    // 'url'   => route($source['route'], $model->id),
                    'url'   =>"/admin/".$source['route']."/".$model->id."",
                    'status'=>$model->{$source['status']},
                    'end'   =>$crudFieldValue1,
                    'dow'   =>$model->{$source['dow']},
                    // 'ranges'=> [
                    //   'dowstart' =>'2021-07-12',
                    //   'dowend'=>'2021-07-15',
                    // ],
           
                    // 
                ];

            }
        }

        return view('admin.calendar.calendar', compact('events', 'venues','user','users','userID','requestIdPicked','VerifyRole'));
    }
}
