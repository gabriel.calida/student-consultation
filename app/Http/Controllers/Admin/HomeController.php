<?php

namespace App\Http\Controllers\Admin;
use Auth;

class HomeController
{
    public function index()
    {
    	$user = Auth::user();
        return view('home', compact('user'));

    }
}
