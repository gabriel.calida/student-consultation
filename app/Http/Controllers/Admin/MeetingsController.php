<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyMeetingRequest;
use App\Http\Requests\StoreMeetingRequest;
use App\Http\Requests\UpdateMeetingRequest;
use App\Role;
use App\Meeting;
use Gate;
use App\User;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use DB;
use Auth;
use Carbon\Carbon;
use Mail;
use App\Mail\SendMailable;

class MeetingsController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('meeting_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        // $meetings = Meeting::all();
        $user = Auth::user();
        $userID = Auth::id();
        // dd($user['id']);
        $VerifyRole = DB::table('role_user')->select('role_id')
        ->where('user_id',$user['id'])
        ->first();

        // dd($VerifyRole->role_id);

        switch ($VerifyRole->role_id) {
            //role student
            case '1':
                $meetings=Meeting::select('meetings.*')
            ->join('users as test1', 'test1.id', '=', 'meetings.attendees')
            ->join('users as test2', 'test2.id', '=', 'meetings.filer_id')
            ->select('test1.name as test1name', 'test2.name as test2name','meetings.start_time','meetings.end_time','meetings.id','meetings.status')
            ->get();
                // dd($meetings);

                break;
            case '4':
                $meetings=Meeting::select('meetings.*')
            ->join('users as test1', 'test1.id', '=', 'meetings.attendees')
            ->join('users as test2', 'test2.id', '=', 'meetings.filer_id')
            ->select('test1.name as test1name', 'test2.name as test2name','meetings.start_time','meetings.end_time','meetings.id','meetings.status')
            ->where('meetings.attendees',$user['id'])
            ->get();
                // dd($meetings);

                break;

            case '5':
                $meetings=Meeting::select('meetings.*')
            ->join('users as test1', 'test1.id', '=', 'meetings.attendees')
            ->join('users as test2', 'test2.id', '=', 'meetings.filer_id')
            ->select('test1.name as test1name', 'test2.name as test2name','meetings.start_time','meetings.end_time','meetings.id','meetings.status')
            ->where('meetings.filer_id',$user['id'])
            ->get();
            break;

            default:
                $meetings = Meeting::all();
                break;
        }

        // dd($meetings);
        

        return view('admin.meetings.index', compact('meetings','user','VerifyRole','userID'));
    }

    public function create()
    {
        abort_if(Gate::denies('meeting_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $attendees = User::where('id','!=','1')->pluck('name', 'id');

        $user = Auth::user();
        // dd($user['id']);
        $VerifyRole = DB::table('role_user')->select('role_id')
        ->where('user_id',$user['id'])
        ->first();

        // dd($VerifyRole);

        // $attendees=User::select('users.*')
        // ->join('role_user','role_user.user_id','users.id')
        // ->select('users.name','users.id')
        // ->where('id','!=',$user['id'])
        // ->where('id','!=','1')
        // ->get();
        // dd($attendees);
        return view('admin.meetings.create',compact('attendees','user'));
    }


        public function Approve(request $data)
    {
        // dd('asdasd');
        // dd($data->action_link);
        $link = $data->action_link;
        if ($data->action_method == 2) {
            $value_method = 2;
        }elseif($data->action_method == 3){
             $value_method = 3;
        }
        // dd($value_method);
        // dd($data['data']);
        Meeting::where('id',$data['data'])
        ->update(['status' => $value_method,'link' => $link]);
        // dd($test);


        $throwFilerId=Meeting::where('id',$data['data'])->first();
        // dd($throwFilerId->filer_id);



        $meetings=Meeting::select('meetings.*')
            ->join('users as test1', 'test1.id', '=', 'meetings.attendees')
            ->join('users as test2', 'test2.id', '=', 'meetings.filer_id')
            ->select('test1.name as test1name', 'test2.name as test2name','test1.email','meetings.start_time','meetings.end_time','meetings.id','meetings.link')
            ->where('meetings.id',$data['data'])
            ->first();
            // dd($meetings['test2name']);

    //          Professor: {{ $meetings['test2name'] }}
    // <br>
    // Attendee: {{ $meetings['test1name'] }}
    // <br>
    // Email of Attendee: {{ $meetings['email'] }}
    // <br>
    // google Meet Link:  {{ $meetings['link'] }}

            // dd(meetings);
        $name = 'Cloudways';
        Mail::to($meetings['email'])->send(new SendMailable($meetings));
   
        return redirect()->route('admin.meetings.index');
    }

    public function mail()
    {
   
        $name = 'Cloudways';
        Mail::to('gjcalida@gmail.com')->send(new SendMailable($name));
   
        return 'Email sent Successfully';
    }

    public function store(StoreMeetingRequest $request)
    {



        if ($request->input('action_quickadd') == true) {
            $time['start'] = $request->input('date').' '. $request->input('quick_start');
            $time['end'] = $request->input('date').' '. $request->input('quick_end');
            // dd($time);
        }else{
             $time['start'] = $request->input('start_time');
             $time['end'] = $request->input('end_time');
            // dd($time);
        }
        $usersGet = $request->get('user');
        // dd($usersGet);
         $user = Auth::user();
        // dd($request->all());
  
        Meeting::insert(['attendees' => $user['id'],'filer_id' => $usersGet,'status' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now(), 'start_time' => $time['start'], 'end_time' => $time['end'] ]);
        return redirect()->route('admin.meetings.index');
    }

    public function edit(Meeting $meeting)
    {
        abort_if(Gate::denies('meeting_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.meetings.edit', compact('meeting'));
    }

    public function update(UpdateMeetingRequest $request, Meeting $meeting)
    {
        $meeting->update($request->all());

        return redirect()->route('admin.meetings.index');
    }

    public function show(Meeting $meeting)
    {
        abort_if(Gate::denies('meeting_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $user = Auth::user();

        $meetings=Meeting::select('meetings.*')
            ->join('users as test1', 'test1.id', '=', 'meetings.attendees')
            ->join('users as test2', 'test2.id', '=', 'meetings.filer_id')
            ->join('status_code as status_name', 'status_name.id', '=', 'meetings.status')
            ->select('test1.name as test1name', 'test2.name as test2name','meetings.start_time','meetings.id','status_name.status_name')
            ->where('meetings.filer_id',$user['id'])
            ->first();
            // dd($meeting);
        return view('admin.meetings.show', compact('meeting','user'));
    }

    public function destroy(Meeting $meeting)
    {
        abort_if(Gate::denies('meeting_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $meeting->delete();

        return back();
    }

    public function massDestroy(MassDestroyMeetingRequest $request)
    {
        Meeting::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
