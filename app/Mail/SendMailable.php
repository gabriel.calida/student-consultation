<?php
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailable extends Mailable
{
    use Queueable, SerializesModels;
    public $meetings;
   
    public function __construct($meetings)
    {
        $this->meetings = $meetings;
    }
    public function build()
    {
        return $this->view('mail.mails');
    }
}