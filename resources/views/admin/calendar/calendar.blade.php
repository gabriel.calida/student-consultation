@extends('layouts.admin')
@section('content')
<h3 class="page-title">{{ trans('global.systemCalendar') }}</h3>
<div class="card">
    <div class="card-header">
        {{ trans('global.systemCalendar') }}
    </div>

    <div class="card-body">
        <form action="{{ route('admin.systemCalendar') }}" method="GET">
            View Schedule of:
            <select name="user_id">
                <option value="">-- all Schedules --</option>
                @foreach($users as $user)
                    <option value="{{ $user->id }}"
                            @if (request('user_id') == $user->id) selected @endif>{{ $user->name }}
                </option>
                @endforeach
            </select>
            <button type="submit" class="btn btn-sm btn-primary">Filter</button>
        </form>

        <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.css' />

        <style type="text/css"> 

        </style><br>
        <div class="row">
<!-- <div class="col-1 p-3 mb-3 "></div> -->
<!-- <div class="col-2 p-3 mb-3 bg-primary text-white">pending</div> -->
<div class="col-3 p-3 mb-3 bg-secondary text-white">Pending</div>
<div class="col-3 p-3 mb-3 bg-success text-white">Approved</div>
<div class="col-3 p-3 mb-3 bg-danger text-white">Holiday</div>
<div class="col-3 p-3 mb-3 bg-warning text-dark">Class</div>
<!-- <div class="col-1 p-3 mb-3 "></div> -->



</div>

<style type="text/css">
    fc-event-time, .fc-event-title {
padding: 0 1px;
white-space: normal;
}
</style>
        <div id='calendar'></div>

        <!-- Button trigger modal -->
<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
  Launch demo modal
</button>
 -->
<!-- Modal -->

<form action="{{ route("admin.meetings.store",) }}" method="POST" enctype="multipart/form-data">
    @csrf
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">File Reservation </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    <!--     <a href="asdasda">asdasd</a> -->
        <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
      </div>
    </div>
  </div>
</div>
</form>

    </div>
</div>
@endsection

@section('scripts')
@parent
<script src="{{ asset('js/moment.min.js') }}"></script>
<script src="{{ asset('js/calendar.min.js') }}"></script>
<script>
    $(document).ready(function () {


            // page is now ready, initialize the calendar...
            events={!! json_encode($events) !!};
            $('#calendar').fullCalendar({
                // put your options and callbacks here


                initialView: 'dayGridMonth',
                allDaySlot: false,
                eventLimit: 6,


                header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay,listDay'

                },


                events: events,


                dayClick: function(date, jsEvent, view) {
                $("#exampleModal").modal("show");
                 $(".modal-body").html("");
              // $(".modal-body").html("<h3>"+date.format()+"</h3>");
              $(".modal-body").html(
                '<label for="exampleInputEmail1">Filed Date</label> <input type=""  class="form-control" id="" readonly name="date" value="'+ date.format() +'"><br>' +
                '<input type="time" name="quick_start" class="form-control" id="" placeholder=""><br>'+
                '<input type="time" name="quick_end" class="form-control" id="end_time" placeholder=""><br>'+
                '<input type="" name="MeetingPersons"  readonly class="form-control" id="end_time" value="{{$requestIdPicked}}" placeholder="pick from view Schedule"><br>'+
                '<input type="" name="action_quickadd" readonly class="form-control" id="end_time" value="true" placeholder="action_quickadd">'
                );
                },

                eventRender: function(event, element) {
                    //status pending
                    if(event.status == "1") {
                    element.css('background-color', '#c8ced3');
                    }else if(event.status == "2") {
                    element.css('background-color', '#4dbd74');
                    }else if(event.status == "3") {
                    element.css('background-color', '#FF0303');
                    }else if(event.status == "8") {
                    element.css('background-color', '#ffc107');
                    }else if(event.status == "9") {
                    element.css('background-color', '#F9CACA');
                    }else if(event.status == "7") {
                    element.css('background-color', '#ffc107');
                    }


            },

            })
        });
</script>
@stop