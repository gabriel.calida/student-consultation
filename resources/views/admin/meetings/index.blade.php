@extends('layouts.admin')
@section('content')
@can('meeting_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            @if($VerifyRole->role_id == 4)
            <a class="btn btn-success" href="{{ route("admin.meetings.create") }}">
                {{ trans('global.add') }} {{ trans('cruds.meeting.title_singular') }}
            </a>
            @else
            @endif
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('cruds.meeting.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-Meeting">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.meeting.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.meeting.fields.attendees') }}
                        </th>
                        <th>
                        {{ trans('cruds.meeting.fields.professor') }}
                        </th>
                        <th>
                            {{ trans('cruds.meeting.fields.start_time') }}
                        </th>
                        <th>
                            {{ trans('cruds.meeting.fields.end_time') }}
                        </th>

                        <th>
                             Status
                        </th>
                       
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($meetings as $key => $meeting)
                        <tr data-entry-id="{{ $meeting->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $meeting->id ?? '' }}
                            </td>
                            <td>
                                {{ $meeting->test1name ?? '' }}
                            </td>
                            <td>
                                 {{ $meeting->test2name ?? '' }}
                            </td>
                            <td>
                                {{ $meeting->start_time ?? '' }}
                            </td>
                            <td>
                                {{ $meeting->end_time ?? '' }}
                            </td>
                            
                                <!-- {{ $meeting->status ?? '' }} -->
                                @if($meeting->status == 1)
                                <td bgcolor="#e8edf2">
                                Pending
                                </td>
                                @elseif($meeting->status == 2)
                                <td bgcolor="#4dbd74">
                                Approved
                                </td>
                                @elseif($meeting->status == 3)
                                <td bgcolor="#FF0303">
                                Decline
                                </td>
                                @else

                                @endif
                            <td>
                        
                                @can('meeting_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.meetings.show', $meeting->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan

                                @can('meeting_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.meetings.edit', $meeting->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan

                                @if($VerifyRole->role_id == 5)
                                <!-- Button trigger modal -->
                                    @if($meeting->status !=2 || $meeting->status == 3)
                               

                                <!-- Button trigger modal -->
                                <form action="test/{{$meeting->id}}" method="GET" onsubmit="return confirm('{{ trans('global.areYouSure') }} ');" style="display: inline-block;">
                                        <input type="hidden" name="action_method" value="3">
                                        <input type="submit" class="btn btn-xs btn-danger" value="Decline">
                                    </form>

                                     <form action="test/{{$meeting->id}}" method="GET" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        
                                        <input type="submit" class="btn btn-xs btn-success" value="Approve">
                                        <input type="hidden" name="action_method" value="2">
                                        <input type="" name="action_link" value="" placeholder="link set here for meeting">
                                    </form>
                                    @else

                                    @endif
                               
                                @else
                                @endif
                                @can('meeting_delete')
                                    <form action="{{ route('admin.meetings.destroy', $meeting->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan

                            </td>

                        </tr>
                    @endforeach


                </tbody>
            </table>
        </div>


    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('meeting_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.meetings.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  $('.datatable-Meeting:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
})

</script>
@endsection