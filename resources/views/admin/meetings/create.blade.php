@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.meeting.title_singular') }}
    </div>

    <div class="card-body">
        <form action="{{ route("admin.meetings.store") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group {{ $errors->has('roles') ? 'has-error' : '' }}">
                <label for="roles">{{ trans('cruds.user.fields.attendees') }}*
                    <!-- <span class="btn btn-info btn-xs select-all">{{ trans('global.select_all') }}</span> -->
                    <!-- <span class="btn btn-info btn-xs deselect-all">{{ trans('global.deselect_all') }}</span></label> -->
                <select name="user" id="user" class="form-control select2" required>
                    @foreach($attendees as $id => $attendees)
                        <option value="{{ $id }}">{{ $attendees }}</option>
                    @endforeach
                </select>
                @if($errors->has('roles'))
                    <em class="invalid-feedback">
                        {{ $errors->first('roles') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.roles_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('start_time') ? 'has-error' : '' }}">
                <label for="start_time">{{ trans('cruds.meeting.fields.start_time') }}</label>
                <input type="text" id="start_time" name="start_time" class="form-control datetime" value="{{ old('start_time', isset($meeting) ? $meeting->start_time : '') }}">
                @if($errors->has('start_time'))
                    <em class="invalid-feedback">
                        {{ $errors->first('start_time') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.meeting.fields.start_time_helper') }}
                </p>
            </div>

            <div class="form-group {{ $errors->has('start_time') ? 'has-error' : '' }}">
                <label for="start_time">{{ trans('cruds.meeting.fields.end_time') }}</label>
                <input type="text" id="end_time" name="end_time" class="form-control datetime" value="{{ old('start_time', isset($meeting) ? $meeting->start_time : '') }}">
                @if($errors->has('start_time'))
                    <em class="invalid-feedback">
                        {{ $errors->first('start_time') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.meeting.fields.start_time_helper') }}
                </p>
            </div>
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>


    </div>
</div>
@endsection